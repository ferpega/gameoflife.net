﻿using System;
using System.Collections.Generic;

namespace GameOfLife
{
    public sealed class ConditionDeadCellsWithThreeNeighbours: ICondition
    {
        public IEnumerable<Cell> GetCellsMatchingCondition(UniverseGrid universe)
        {
            Func<Cell, int, bool> condition = (cell, neigbours) => !cell.IsAlive && neigbours == 3;
            return universe.GetCellsInUniverseWithConditions(condition);
        }
    }
}
