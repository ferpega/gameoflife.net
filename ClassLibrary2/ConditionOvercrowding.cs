﻿using System;
using System.Collections.Generic;

namespace GameOfLife
{
    public sealed class ConditionOvercrowding: ICondition
    {
        public IEnumerable<Cell> GetCellsMatchingCondition(UniverseGrid universe)
        {
            Func<Cell, int, bool> condition = (cell, neighbours) => cell.IsAlive && neighbours > 3;
            return universe.GetCellsInUniverseWithConditions(condition);
        }
    }
}
