﻿using System;
using System.Collections.Generic;

namespace GameOfLife
{
    public sealed class ConditionUnderpopulation: ICondition
    {
        public IEnumerable<Cell> GetCellsMatchingCondition(UniverseGrid universe)
        {
            Func<Cell, int, bool> condition = (cell, neighbours) => cell.IsAlive && neighbours < 2;
            return universe.GetCellsInUniverseWithConditions(condition);
        }
    }
}
