﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace GameOfLife
{
    public class UniverseGrid
    {
        public UniverseGrid(int horizontalSize, int verticalSize)
        {
            if (horizontalSize < 3 || verticalSize < 3)
                throw new IndexOutOfRangeException("Universe must be of 3x3 Grid at minimum");

            this.horSize = horizontalSize;
            this.verSize = verticalSize;
            cells = new Cell[horizontalSize, verticalSize];
            for (int x = 0; x < cells.GetLength(0); x++) {
                for (int y = 0; y < cells.GetLength(1); y++) {
                    cells[x, y] = new Cell();
                }
            }
        }

        private int horSize;
        public int HorizontalSize
        {
            get { return horSize; }
        }

        private int verSize;
        public int VerticalSize
        {
            get { return verSize; }
        }

        private Cell[,] cells;
        public Cell this[int inColumn, int inRow]
        {
            get
            {
                inColumn--;
                inRow--;
                if (inColumn < 0) return null;
                if (inColumn > HorizontalSize - 1) return null;
                if (inRow < 0) return null;
                if (inRow > HorizontalSize - 1) return null;
                return cells[inColumn, inRow];
            }
        }

        public IEnumerable<Cell> GetAliveNeighboursOf(int inColumn, int inRow)
        {
            var livingNeighbors = from n in this.GetNeighbours(inColumn, inRow)
                                  where n.IsAlive
                                  select n;
            return livingNeighbors;
        }

        public void SetUniverseStatus(string[] map)
        {
            if (map.Count() != this.HorizontalSize) throw new IndexOutOfRangeException("Horizontal size is different than map horizontal size.");
            if (map[0].Length != this.VerticalSize) throw new IndexOutOfRangeException("Vertical size is different than map vertical size.");

            for (int h = 0; h < map[0].Length; h++) {
                for (int v = 0; v < map.Count(); v++) {
                    this[h + 1, v + 1].IsAlive = map[v].Substring(h,1) != ".";
                }
            }
        }

        internal IEnumerable<Cell> GetCellsInUniverseWithConditions(Func<Cell, int, bool> conditionCell)
        {
            IList<Cell> result = new List<Cell>();
            for (int h = 1; h <= cells.GetLength(0); h++)
                for (int v = 1; v <= cells.GetLength(1); v++) {
                    if (conditionCell(this[h, v], this.GetAliveNeighboursOf(h, v).Count())) result.Add(this[h, v]);
                }
            return result;
        }

        private IEnumerable<Cell> GetNeighbours(int inColumn, int inRow)
        {
            IList<Cell> result = new List<Cell>();

            var previousRow = inRow - 1;
            var nextRow = inRow + 1;
            var previousColumn = inColumn - 1;
            var nextColumn = inColumn + 1;

            AddToCollectionIfNotNull(result, this[previousColumn, previousRow]);
            AddToCollectionIfNotNull(result, this[inColumn, previousRow]);
            AddToCollectionIfNotNull(result, this[nextColumn, previousRow]);

            AddToCollectionIfNotNull(result, this[previousColumn, inRow]);
            AddToCollectionIfNotNull(result, this[nextColumn, inRow]);

            AddToCollectionIfNotNull(result, this[previousColumn, nextRow]);
            AddToCollectionIfNotNull(result, this[inColumn, nextRow]);
            AddToCollectionIfNotNull(result, this[nextColumn, nextRow]);

            return result;
        }

        private void AddToCollectionIfNotNull(IList<Cell> resultNeighbors, Cell cell)
        {
            if (cell != null) resultNeighbors.Add(cell);
        }

    }
}

