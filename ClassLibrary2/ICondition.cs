﻿using System.Collections.Generic;

namespace GameOfLife
{
    public interface ICondition
    {
        IEnumerable<Cell> GetCellsMatchingCondition(UniverseGrid universe);
    }
}
