﻿namespace GameOfLife
{
    public class Cell
    {
        public Cell() : this(false) { }
        public Cell(bool isAlive)
        {
            this.isAlive = isAlive;
        }

        private bool isAlive;
        public bool IsAlive
        {
            get { return isAlive; }
            set { this.isAlive = value; }
        }
    }
}
