﻿using System.Collections.Generic;

namespace GameOfLife
{
    public class TransitionDaemon
    {
        private UniverseGrid universe;
        public TransitionDaemon(UniverseGrid universe)
        {
            this.universe = universe;
        }

        public void DoIterationOnUniverse()
        {
            var cellsToDieUnderpopulation = new ConditionUnderpopulation().GetCellsMatchingCondition(universe);
            var cellsToLiveNextGeneration = new ConditionTwoOrThreeNeighbours().GetCellsMatchingCondition(universe);
            var cellsToDieOvercrowding = new ConditionOvercrowding().GetCellsMatchingCondition(universe);
            var cellsToRevive = new ConditionDeadCellsWithThreeNeighbours().GetCellsMatchingCondition(universe);

            SetStatusOfCells(cellsToDieUnderpopulation, false);
            //SetStatusOfCells(cellsToLiveNextGeneration, true);
            SetStatusOfCells(cellsToDieOvercrowding, false);
            SetStatusOfCells(cellsToRevive, true);
        }

        private void SetStatusOfCells(IEnumerable<Cell> cells, bool isAlive)
        {
            foreach (var cell in cells) {
                cell.IsAlive = isAlive;
            }
        }
    }
}
