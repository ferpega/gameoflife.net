﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using GameOfLife;

namespace GameOfLifeTests
{
    [TestClass]
    class TestsCell
    {
        [Test]
        public void Cell_DefaultConstructor()
        {
            var cell = new Cell();
            Assert.That(cell.IsAlive, Is.False);
        }
        [Test]
        public void Cell_ConstructorWithCellAlive()
        {
            var cell = new Cell(true);
            Assert.That(cell.IsAlive, Is.True);
        }
        [Test]
        public void Cell_ConstructorWithDeadCell()
        {
            var cell = new Cell(false);
            Assert.That(cell.IsAlive, Is.False);
        }
    }
}
