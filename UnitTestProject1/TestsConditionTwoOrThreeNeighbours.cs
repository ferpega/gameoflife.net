﻿using GameOfLife;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System.Linq;
using FluentAssertions;

namespace GameOfLifeTests
{
    [TestClass]
    public class TestsConditionTwoOrThreeNeighbours
    {
        [TestMethod]
        public void CheckCells_With_Two_Or_Three_Neighbours()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive = true;
            universe[2, 2].IsAlive = true;
            universe[3, 3].IsAlive = true;

            var condition = new ConditionTwoOrThreeNeighbours();
            var resultCells = condition.GetCellsMatchingCondition(universe);
            resultCells.Count().Should().Be(1);
        }
    }
}
