﻿
using GameOfLife;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using FluentAssertions;

namespace GameOfLifeTests
{
    [TestClass]
    class TestsCell
    {
        [TestMethod]
        public void Cell_DefaultConstructor()
        {
            var cell = new Cell();
            cell.IsAlive.Should().BeFalse();
        }
        [TestMethod]
        public void Cell_ConstructorWithCellAlive()
        {
            var cell = new Cell(true);
            cell.IsAlive.Should().BeTrue();
        }
        [TestMethod]
        public void Cell_ConstructorWithDeadCell()
        {
            var cell = new Cell(false);
            cell.IsAlive.Should().BeFalse();
        }
    }
}
