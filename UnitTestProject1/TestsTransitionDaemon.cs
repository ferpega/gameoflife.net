﻿using GameOfLife;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System.Linq;
using FluentAssertions;

namespace GameOfLifeTests
{
    [TestClass]
    public class TestsTransitionDaemon
    {
        [TestMethod]
        public void StatusAfterIterationOne()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive = true;
            universe[2, 1].IsAlive = true;
            universe[3, 1].IsAlive = false;
            universe[1, 2].IsAlive = true;
            universe[2, 2].IsAlive = true;
            universe[3, 2].IsAlive = false;
            universe[1, 3].IsAlive = false;
            universe[2, 3].IsAlive = false;
            universe[3, 3].IsAlive = true;

            var daemon = new TransitionDaemon(universe);
            daemon.DoIterationOnUniverse();
            universe[1, 1].IsAlive.Should().BeTrue();
            universe[2, 1].IsAlive.Should().BeTrue();
            universe[3, 1].IsAlive.Should().BeFalse();

            universe[1, 2].IsAlive.Should().BeTrue();
            universe[2, 2].IsAlive.Should().BeFalse();
            universe[3, 2].IsAlive.Should().BeTrue();

            universe[1, 3].IsAlive.Should().BeFalse();
            universe[2, 3].IsAlive.Should().BeTrue();
            universe[3, 3].IsAlive.Should().BeFalse();
        }

        [TestMethod]
        public void StatusAfterIterationTwo()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive = true;
            universe[2, 1].IsAlive = true;
            universe[3, 1].IsAlive = false;
            universe[1, 2].IsAlive = true;
            universe[2, 2].IsAlive = false;
            universe[3, 2].IsAlive = true;
            universe[1, 3].IsAlive = false;
            universe[2, 3].IsAlive = true;
            universe[3, 3].IsAlive = false;

            var daemon = new TransitionDaemon(universe);
            daemon.DoIterationOnUniverse();
            universe[1, 1].IsAlive.Should().BeTrue();
            universe[2, 1].IsAlive.Should().BeTrue();
            universe[3, 1].IsAlive.Should().BeFalse();

            universe[1, 2].IsAlive.Should().BeTrue();
            universe[2, 2].IsAlive.Should().BeFalse();
            universe[3, 2].IsAlive.Should().BeTrue();

            universe[1, 3].IsAlive.Should().BeFalse();
            universe[2, 3].IsAlive.Should().BeTrue();
            universe[3, 3].IsAlive.Should().BeFalse();
        }

        [TestMethod]
        public void Oscilator_Blinker()
        {
            var universe = new UniverseGrid(5, 5);
            #region[Expectations]
            string[] expectedIteration1 = new string[5]
            {
                ".....",
                "..#..",
                "..#..",
                "..#..",
                ".....",
            };
            string[] expectedIteration2 = new string[5]
            {
                ".....",
                ".....",
                ".###.",
                ".....",
                ".....",
            };
            #endregion
            universe.SetUniverseStatus(expectedIteration1);

            var daemon = new TransitionDaemon(universe);
            daemon.DoIterationOnUniverse();
            CheckUniverseCells(universe, expectedIteration2, "Blinker iteration 2");

            daemon.DoIterationOnUniverse();
            CheckUniverseCells(universe, expectedIteration1, "Blinker iteration 3");
        }

        [TestMethod]
        public void Oscilator_Toad()
        {
            var universe = new UniverseGrid(6, 6);
            #region[Expectations]
            string[] expectedIteration1 = new string[6] 
            {   "......",
                "......",
                "..###.",
                ".###..",
                "......",
                "......"
            };
            string[] expectedIteration2 = new string[6] 
            {   "......",
                "...#..",
                ".#..#.",
                ".#..#.",
                "..#...",
                "......"
            };
            #endregion
            universe.SetUniverseStatus(expectedIteration1);

            var daemon = new TransitionDaemon(universe);
            daemon.DoIterationOnUniverse();
            CheckUniverseCells(universe, expectedIteration2, "Toad iteration 2");

            daemon.DoIterationOnUniverse();
            CheckUniverseCells(universe, expectedIteration1, "Toad iteration 3");
        }

        [TestMethod]
        public void Oscilator_Pulsar()
        {
            var universe = new UniverseGrid(17, 17);
            #region[Expectations]
            string[] expectedIteration1 = new string[17] 
            {   ".................",
                ".................",
                "....###...###....",
                ".................",
                "..#....#.#....#..",
                "..#....#.#....#..",
                "..#....#.#....#..",
                "....###...###....",
                ".................",
                "....###...###....",
                "..#....#.#....#..",
                "..#....#.#....#..",
                "..#....#.#....#..",
                ".................",
                "....###...###....",
                ".................",
                "................."
            };
            string[] expectedIteration2 = new string[17] 
            {   ".................",
                ".....O.....O.....",
                ".....O.....O.....",
                ".....OO...OO.....",
                ".................",
                ".OOO..OO.OO..OOO.",
                "...O.O.O.O.O.O...",
                ".....OO...OO.....",
                ".................",
                ".....OO...OO.....",
                "...O.O.O.O.O.O...",
                ".OOO..OO.OO..OOO.",
                ".................",
                ".....OO...OO.....",
                ".....O.....O.....",
                ".....O.....O.....",
                "................."
            };
            string[] expectedIteration3 = new string[17] 
            {   ".................",
                ".................",
                "....11.....11....",
                ".....11...11.....",
                "..1..1.1.1.1..1..",
                "..111.11.11.111..",
                "...1.1.1.1.1.1...",
                "....111...111....",
                ".................",
                "....111...111....",
                "...1.1.1.1.1.1...",
                "..111.11.11.111..",
                "..1..1.1.1.1..1..",
                ".....11...11.....",
                "....11.....11....",
                ".................",
                "................."
            };
            #endregion
            universe.SetUniverseStatus(expectedIteration1);

            var daemon = new TransitionDaemon(universe);
            daemon.DoIterationOnUniverse();
            CheckUniverseCells(universe, expectedIteration2, "Pulsar iteration 2");

            daemon.DoIterationOnUniverse();
            CheckUniverseCells(universe, expectedIteration3, "Pulsar iteration 3");
        }

        private void CheckUniverseCells(UniverseGrid universe, string[] expected, string iterationMessage)
        {
            for (int h = 0; h < expected[0].Length; h++) {
                for (int v = 0; v < expected.Count(); v++) {
                    universe[h+1, v+1].IsAlive.Should().Be(expected[v].Substring(h,1) != ".", 
                        "[{4}] - Position ({0},{1}), value: {2}, expected: {3}",
                                    h+1, v+1, universe[h+1, v+1].IsAlive, expected[v].Substring(h,1) != ".", iterationMessage);
                }                
            }
        }

    }
}
