﻿using GameOfLife;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System.Linq;
using FluentAssertions;

namespace GameOfLifeTests
{
    [TestClass]
    public class TestsConditionOvercrowding
    {
        [TestMethod]
        public void CheckCells_With_Overcrowding_Neighbours()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive = true;
            universe[2, 1].IsAlive = true;
            universe[1, 2].IsAlive = true;
            universe[2, 2].IsAlive = true;
            universe[3, 3].IsAlive = true;

            var condition = new ConditionOvercrowding();
            var resultCells = condition.GetCellsMatchingCondition(universe);
            resultCells.Count().Should().Be(1);
        }
    }
}
