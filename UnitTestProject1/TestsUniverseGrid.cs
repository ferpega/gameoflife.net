﻿using GameOfLife;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;
using System.Collections.Generic;
using System.Linq;
using FluentAssertions;

namespace GameOfLifeTests
{
    [TestClass]
    public class TestsUniverseGrid
    {
        [TestMethod]
        public void CreationCheckSize()
        {
            var universe = new UniverseGrid(3, 3);
            universe.HorizontalSize.Should().Be(3);
            universe.VerticalSize.Should().Be(3);
        }
        [TestMethod]
        public void Creation_CellsStatusDead()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive.Should().BeFalse();
        }

        [TestMethod]
        public void Creation_CellsStatusDeadIsChangedToAlive()
        {
            var universe = new UniverseGrid(3, 3);
            universe[2, 2].IsAlive.Should().BeFalse();
            universe[2, 2].IsAlive = true;
            universe[2, 2].IsAlive.Should().BeTrue();
        }

        [TestMethod]
        public void GetNeighboursAlive_CentralSquare()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive = true;
            universe[2, 2].IsAlive = true;
            universe[3, 3].IsAlive = true;
            IEnumerable<Cell> livingNeighbors = universe.GetAliveNeighboursOf(2, 2);

            livingNeighbors.Count().Should().Be(2);

            universe[2, 1].IsAlive = true;
            universe[3, 2].IsAlive = true;
            universe[1, 3].IsAlive = true;
            livingNeighbors = universe.GetAliveNeighboursOf(2, 2);

            livingNeighbors.Count().Should().Be(5);
        }

        [TestMethod]
        public void GetLivingNeighbors_CornerSquare_LeftUp()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive = true;
            universe[2, 2].IsAlive = true;
            universe[3, 3].IsAlive = true;
            IEnumerable<Cell> livingNeighbors = universe.GetAliveNeighboursOf(1, 1);

            livingNeighbors.Count().Should().Be(1);

            universe[2, 1].IsAlive = true;
            universe[3, 2].IsAlive = true;
            universe[1, 3].IsAlive = true;
            livingNeighbors = universe.GetAliveNeighboursOf(1, 1);

            livingNeighbors.Count().Should().Be(2);

            universe[1, 2].IsAlive = true;
            livingNeighbors = universe.GetAliveNeighboursOf(1, 1);

            livingNeighbors.Count().Should().Be(3);
        }

        [TestMethod]
        public void GetLivingNeighbors_CornerSquare_RightUp()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive = true;
            universe[2, 2].IsAlive = true;
            universe[3, 3].IsAlive = true;
            IEnumerable<Cell> livingNeighbors = universe.GetAliveNeighboursOf(1, 3);

            livingNeighbors.Count().Should().Be(1);

            universe[2, 1].IsAlive = true;
            universe[3, 2].IsAlive = true;
            universe[1, 3].IsAlive = true;
            livingNeighbors = universe.GetAliveNeighboursOf(1, 1);

            livingNeighbors.Count().Should().Be(2);

            universe[1, 2].IsAlive = true;
            livingNeighbors = universe.GetAliveNeighboursOf(1, 1);

            livingNeighbors.Count().Should().Be(3);
        }

        [TestMethod]
        public void GetLivingNeighbors_CornerSquare_LeftDown()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive = true;
            universe[2, 2].IsAlive = true;
            universe[3, 3].IsAlive = true;
            IEnumerable<Cell> livingNeighbors = universe.GetAliveNeighboursOf(1, 3);

            livingNeighbors.Count().Should().Be(1);

            universe[2, 3].IsAlive = true;
            universe[3, 2].IsAlive = true;
            universe[1, 3].IsAlive = true;
            livingNeighbors = universe.GetAliveNeighboursOf(1, 3);

            livingNeighbors.Count().Should().Be(2);

            universe[1, 2].IsAlive = true;
            livingNeighbors = universe.GetAliveNeighboursOf(1, 3);

            livingNeighbors.Count().Should().Be(3);
        }

        [TestMethod]
        public void GetLivingNeighbors_CornerSquare_RightDown()
        {
            var universe = new UniverseGrid(3, 3);
            universe[1, 1].IsAlive = true;
            universe[2, 2].IsAlive = true;
            universe[3, 3].IsAlive = true;
            IEnumerable<Cell> livingNeighbors = universe.GetAliveNeighboursOf(3, 3);

            livingNeighbors.Count().Should().Be(1);

            universe[2, 3].IsAlive = true;
            universe[3, 2].IsAlive = true;
            universe[1, 3].IsAlive = true;
            livingNeighbors = universe.GetAliveNeighboursOf(3, 3);

            livingNeighbors.Count().Should().Be(3);

            universe[2, 2].IsAlive = false;
            livingNeighbors = universe.GetAliveNeighboursOf(3, 3);

            livingNeighbors.Count().Should().Be(2);
        }
    }
}
